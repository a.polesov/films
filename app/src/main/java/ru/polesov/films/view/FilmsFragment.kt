package ru.polesov.films.view

import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.fragment_films.*

import ru.polesov.films.R
import ru.polesov.films.data.entity.Film
import ru.polesov.films.view.adapter.FilmsAdapter
import ru.polesov.films.viewmodel.FilmsViewModel

class FilmsFragment : Fragment(), FilmsAdapter.OnItemClickListener {

    private lateinit var viewModel: FilmsViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProviders.of(this)[FilmsViewModel::class.java]
        setHasOptionsMenu(true)
    }

    override fun onCreateView(inflater: LayoutInflater,container: ViewGroup?,savedInstanceState: Bundle?): View? {

        return inflater.inflate(R.layout.fragment_films, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        //При swipe списка популярных фильмов делаем запрос в сеть
        swipeRefresh.setOnRefreshListener {
            observePopularFilms()
        }

        val linearLayoutManager = LinearLayoutManager(context)
        PopularFilmsRecycleView.layoutManager = linearLayoutManager
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        observePopularFilms()
    }

    // Получаем данные из сети, затем сравниваем с БД кто избранный, выводим в RecyclerView
    private fun observePopularFilms() {
        visibilityProgressBarWhenLoading(true)
        viewModel.getFilmsNetwork().observe(this,
            Observer<List<Film>> { t ->
                t?.let { networkFilmsList ->
                    networkFilmsList.forEach { networkFilm ->
                        networkFilm.id?.let {
                            networkFilm.isFavourites = viewModel.isFavourites(it)
                        }
                    }
                    val adapter = FilmsAdapter(networkFilmsList, this)
                    PopularFilmsRecycleView.adapter = adapter
                    visibilityProgressBarWhenLoading(false)
                    swipeRefresh.isRefreshing = false
                }
            })
    }

    // Обработчик нажатия на элемент списка популярных фильмов
    override fun onItemClick(id: Int) {
        val action = FilmsFragmentDirections.openDetailFilms()
        action.id = id
        action.network = true
        findNavController().navigate(action)
    }

     private fun visibilityProgressBarWhenLoading(startLoadIsNetwork: Boolean){
         if (startLoadIsNetwork) {
             if (!swipeRefresh.isRefreshing)
                 progressPopularFilms.visibility = View.VISIBLE
             else
                 progressPopularFilms.visibility = View.GONE

             PopularFilmsRecycleView.visibility = View.GONE
         }
         else{
             progressPopularFilms.visibility = View.GONE
             PopularFilmsRecycleView.visibility = View.VISIBLE
         }
     }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.toolbar_menu, menu)

        val searchItem = menu.findItem(R.id.action_search)
        val searchView = searchItem.actionView as SearchView
        searchView.setQueryHint("Search View Hint")

        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {

            override fun onQueryTextChange(newText: String): Boolean {
                return false
            }

            override fun onQueryTextSubmit(query: String): Boolean {
                Log.d("Search", "Search onQueryTextSubmit")
                return false
            }

        })

        super.onCreateOptionsMenu(menu, inflater)

    }
}


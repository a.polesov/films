package ru.polesov.films.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide

import ru.polesov.films.data.entity.Film
import ru.polesov.films.databinding.RecyclerviewItemFilmsBinding

class FilmsAdapter(
    private var items: List<Film>,
    private var listener: OnItemClickListener
) : RecyclerView.Adapter<FilmsAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = RecyclerviewItemFilmsBinding.inflate(layoutInflater, parent, false)
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(items[position], listener)
    }

    interface OnItemClickListener {
        fun onItemClick(id: Int)
    }

    class ViewHolder(private var item: RecyclerviewItemFilmsBinding) : RecyclerView.ViewHolder(item.root) {
        fun bind(item_film: Film, listener: OnItemClickListener) {
            // Bind объекта
            item.film = item_film

            // Привязываем обработчик по нажатию на элемент
            item.film?.id?.let {
                item.root.setOnClickListener { _ -> listener.onItemClick(it) }
            }
            // Тянем изображение для ImageView
            item.film?.backdropPath?.let {
                val url = "https://image.tmdb.org/t/p/w500$it"
                Glide.with(item.imagePreview.context)
                    .load(url)
                    .into(item.imagePreview)
            }
            item.executePendingBindings()
        }
    }
}
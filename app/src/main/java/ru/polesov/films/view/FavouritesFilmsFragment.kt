package ru.polesov.films.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.fragment_favourites_films.*
import ru.polesov.films.R
import ru.polesov.films.data.entity.Film
import ru.polesov.films.view.adapter.FilmsAdapter
import ru.polesov.films.viewmodel.FavouritesViewModel

class FavouritesFilmsFragment : Fragment(), FilmsAdapter.OnItemClickListener {

    private lateinit var viewModel: FavouritesViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProviders.of(this)[FavouritesViewModel::class.java]
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_favourites_films, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val linearLayoutManager = LinearLayoutManager(context)
        FavouriteFilmsRecycleView.layoutManager = linearLayoutManager
        observeFavouritesFilms()
    }

    // Получаем данные из БД, отображает в RecyclerView
    private fun observeFavouritesFilms() {
        viewModel.getFilmsFavourites().observe(this,
            Observer<List<Film>> { t ->
                t?.let {
                    val adapter = FilmsAdapter(it, this)
                    FavouriteFilmsRecycleView.adapter = adapter
                    FavouriteFilmsRecycleView.visibility = View.VISIBLE
                    progressFavouriteFilms.visibility = View.GONE
                }
            })
    }

    // Обработчик нажатия на элемент списка избранных фильмов
    override fun onItemClick(id: Int) {
        val action = FavouritesFilmsFragmentDirections.openDetailFragment()
        action.id = id
        action.network = false
        findNavController().navigate(action)
    }

}
package ru.polesov.films.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.fragment_detail.*
import ru.polesov.films.R
import ru.polesov.films.data.entity.Film
import ru.polesov.films.databinding.FragmentDetailBinding
import ru.polesov.films.viewmodel.DetailViewModel

class DetailFragment : Fragment() {

    private lateinit var viewModel: DetailViewModel
    private lateinit var binding: FragmentDetailBinding
    private var idFilm: Int = 0
    private var loadNetwork: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProviders.of(this)[DetailViewModel::class.java]
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_detail, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setTitleToolBar("")

        // Получаем Id по которому нажали, и признак данных Сеть/БД
        arguments?.let {
            idFilm = DetailFragmentArgs.fromBundle(it).id
            loadNetwork = DetailFragmentArgs.fromBundle(it).network
            observeFilmOnID(idFilm, loadNetwork)
        }

        button_favourite.setOnClickListener {
            viewModel.addOrDeleteToFavourites()
           // observeFilmOnID(idFilm, loadNetwork)
            binding.invalidateAll()
        }

    }

    // Получаем данные из Id, либо из сети либо из БД, подгружаем изображение из сети
    private fun observeFilmOnID(id: Int, network: Boolean) {
        visibleViewNetworkLoad(network)
        viewModel.getFilmOnId(id, network).observe(this,
            Observer<Film> { t ->
                t?.let { film ->
                    binding.film = film
                    binding.invalidateAll()
                    film.backdropPath?.let {
                        val url = "https://image.tmdb.org/t/p/w500$it"
                        Glide.with(this)
                            .load(url)
                            .into(imageDetail)
                    }
                    film.title?.let { title ->
                        setTitleToolBar(title)
                    }
                    visibleViewNetworkLoad(false)
                }
            })
    }

    //Если загрузка то скрываем View, отображаем ProgressBar
    private fun visibleViewNetworkLoad(loadStart: Boolean) {
        if (loadStart) {
            button_favourite.visibility = View.GONE
            detail_view.visibility = View.GONE
            progressDetail.visibility = View.VISIBLE
        } else {
            button_favourite.visibility = View.VISIBLE
            detail_view.visibility = View.VISIBLE
            progressDetail.visibility = View.GONE
        }
    }

    // Задаем заголовок
    private fun setTitleToolBar(title: String) {
        (activity as AppCompatActivity).supportActionBar?.title = title

    }
}
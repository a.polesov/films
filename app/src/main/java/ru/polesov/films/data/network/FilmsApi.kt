package ru.polesov.films.data.network

import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query
import ru.polesov.films.data.entity.Film
import ru.polesov.films.data.entity.FilmResponse

interface FilmsApi {

    @GET("/4/discover/movie/")
    fun getPopularFilms(
        @Query("api_key") api_key: String,
        @Query("sort_by") sort: String,
        @Query("language") language: String
    ): Single<FilmResponse>

    @GET("/3/movie/{id}")
    fun getFilm(
        @Path("id") id: Int,
        @Query("api_key") api_key: String,
        @Query("language") language: String
    ): Single<Film>

}


package ru.polesov.films.data.common

class Constants {
    companion object{
        const val urlFilms = "https://api.themoviedb.org"
        const val api_key = "5f0fdd58d335c5a2827f24a2cf71856c"
        const val sort = "popularity.desc"
        const val language = "ru"
    }
}
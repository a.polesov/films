package ru.polesov.films.data.repository

import io.reactivex.Single
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import ru.polesov.films.data.entity.Film
import ru.polesov.films.data.entity.FilmResponse
import ru.polesov.films.data.network.FilmsApi
import ru.polesov.films.data.common.Constants


class FilmsNetwork {

    private fun getApi(): FilmsApi {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constants.urlFilms)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()
        return retrofit.create(FilmsApi::class.java)
    }

    fun getPopularFilmsResponse(): Single<FilmResponse> {
        return getApi().getPopularFilms(Constants.api_key, Constants.sort, Constants.language)
    }

    fun getFilmOnId(id: Int): Single<Film> {
        return getApi().getFilm(id, Constants.api_key, Constants.language)
    }


}
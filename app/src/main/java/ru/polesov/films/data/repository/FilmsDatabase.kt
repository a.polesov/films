package ru.polesov.films.data.repository

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import io.realm.Realm
import io.realm.kotlin.where
import ru.polesov.films.data.entity.Film

class FilmsDatabase {

    // Получаем список фильмов из БД
    fun getFilmsDatabase(): LiveData<List<Film>> {
        val films = MutableLiveData<List<Film>>()
        val realm = Realm.getDefaultInstance()
        val filmsDB: List<Film> = realm.where<Film>().equalTo("isFavourites", true).findAll()
        films.postValue(filmsDB)
        return films
    }

    // Получаем фильм по Id из БД
    fun getFilmDatabase(id: Int): Film? {
        val realm = Realm.getDefaultInstance()
        val film = realm.where<Film>().equalTo("id", id).findFirst()
        return realm.copyFromRealm(film)
    }

    //  Проверяем  избранный ли фильм по Id из БД
    fun isFavourites(id: Int): Boolean {
        var isFavourites = false
        val realm = Realm.getDefaultInstance()
        val filmsDB: List<Film> = realm.where<Film>()
            .equalTo("id", id)
            .equalTo("isFavourites", true)
            .findAll()
        if (filmsDB.count() > 0) {
            isFavourites = true
        }
        realm.close()
        return isFavourites
    }

    // Создаем новый объект Film и копируем данные
    fun addFavoriteFilmToDB(newFilm: Film) {
        val realm = Realm.getDefaultInstance()
        realm.beginTransaction()
        val findFilms = realm.where<Film>().equalTo("id", newFilm.id).findAll()
        findFilms.deleteAllFromRealm()
        val film = realm.createObject(Film::class.java, newFilm.id)
       // film.id = newFilm.id
        film.popularity = newFilm.popularity
        film.video = newFilm.video
        film.voteCount = newFilm.voteCount
        film.voteAverage = newFilm.voteAverage
        film.title = newFilm.title
        film.releaseDate = newFilm.releaseDate
        film.originalLanguage = newFilm.originalLanguage
        film.originalTitle = newFilm.originalTitle
        film.backdropPath = newFilm.backdropPath
        film.adult = newFilm.adult
        film.overview = newFilm.overview
        film.posterPath = newFilm.posterPath
        film.isFavourites = newFilm.isFavourites
        realm.commitTransaction()
        realm.close()
        Log.d("database", "Database add new film")
    }

    // Удаляем объект из бд по id
    fun deleteFavoritesFilmFromDB(deleteFilmId: Int) {
        val realm = Realm.getDefaultInstance()
        val film = realm.where<Film>().equalTo("id", deleteFilmId).findFirst()
        realm.beginTransaction()
        film?.deleteFromRealm()
        realm.commitTransaction()
        realm.close()
        Log.d("database", "Database delete new film")
    }


}
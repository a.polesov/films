package ru.polesov.films.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import ru.polesov.films.data.entity.Film
import ru.polesov.films.data.repository.FilmsDatabase

class FavouritesViewModel : ViewModel() {

    private var repositoryDatabase: FilmsDatabase = FilmsDatabase()

    // Чистим БД от неактуальных записей и получаем список избранных фильмов
    fun getFilmsFavourites(): LiveData<List<Film>> {
      //  repositoryDatabase.deleteAllFilmsNotFavoritesFromDB()
        return repositoryDatabase.getFilmsDatabase()
    }
}
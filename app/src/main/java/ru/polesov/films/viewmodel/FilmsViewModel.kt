package ru.polesov.films.viewmodel

import android.annotation.SuppressLint
import androidx.lifecycle.*
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import ru.polesov.films.data.entity.Film
import ru.polesov.films.data.repository.FilmsDatabase
import ru.polesov.films.data.repository.FilmsNetwork

class FilmsViewModel : ViewModel() {

    private var repositoryNetwork: FilmsNetwork = FilmsNetwork()
    private var repositoryDatabase: FilmsDatabase = FilmsDatabase()

    // Получаем список популярных фильмов из сети
    @SuppressLint("CheckResult")
    fun getFilmsNetwork(): LiveData<List<Film>> {
        val data = MutableLiveData<List<Film>>()
        repositoryNetwork.getPopularFilmsResponse()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(
                onSuccess = { response ->
                    response.films?.let { list ->
                        data.postValue(list)
                    }
                })
        return data
    }

    // Проверяем избранный ли фильм БД
    fun isFavourites(id: Int): Boolean {
        return repositoryDatabase.isFavourites(id)
    }
}
package ru.polesov.films.viewmodel

import android.annotation.SuppressLint
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import ru.polesov.films.data.entity.Film
import ru.polesov.films.data.repository.FilmsDatabase
import ru.polesov.films.data.repository.FilmsNetwork

class DetailViewModel : ViewModel() {

    private var filmLiveData = MutableLiveData<Film>()

    private var repositoryDatabase: FilmsDatabase = FilmsDatabase()

    // Запрос из репозитория фильма по ID
    @SuppressLint("CheckResult")
    fun getFilmOnId(id: Int, network: Boolean): LiveData<Film> {
        if (network) {
            // Берем данные из сетив
            if (filmLiveData.value == null) {
                // Если в Livedata фильма нет, тянем из сети
                val repositoryNetwork = FilmsNetwork()
                repositoryNetwork.getFilmOnId(id)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeBy(
                        onSuccess = { response ->
                            response?.let { film ->
                                film.id?.let { film.isFavourites = repositoryDatabase.isFavourites(it) }
                                filmLiveData.postValue(film)
                            }
                        })
            } else {
                // Если Livedata фильм есть, то используем его но проверяем "избранность"
                filmLiveData.value?.let { film ->
                    film.id?.let {
                        if (film.isFavourites != repositoryDatabase.isFavourites(it))
                            checkRelevanceFavourites(film)
                    }
                }
            }
        } else {
            // Берем данные из базы данных
            if (filmLiveData.value == null)
                filmLiveData.postValue(repositoryDatabase.getFilmDatabase(id))
        }
        return filmLiveData
    }

    //  Добавляет или удаляет признак избранного в БД
    fun addOrDeleteToFavourites() {
        filmLiveData.value?.isFavourites?.let { isFavourite ->
            if (isFavourite)
                filmLiveData.value?.id?.let {
                    filmLiveData.value?.isFavourites = false
                    repositoryDatabase.deleteFavoritesFilmFromDB(it)
                }
            else {
                filmLiveData.value?.isFavourites = true
                filmLiveData.value?.let { repositoryDatabase.addFavoriteFilmToDB(it) }
            }
        }
    }

    // Проверяем чтобы не было расхождение избранности
    private fun checkRelevanceFavourites(film: Film) {
        film.id?.let {
            if (repositoryDatabase.isFavourites(it)) {
                filmLiveData.postValue(repositoryDatabase.getFilmDatabase(it))
            } else {
                film.isFavourites = repositoryDatabase.isFavourites(it)
                filmLiveData.postValue(film)
            }
        }

    }

}

